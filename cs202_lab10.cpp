#include "table.h"
using namespace std;

//These are the functions that you will be writing for Lab #10

//STEP 1
int table::count()	//SAMPLE wrapper function
{
	return count(root);
}

int table::count(node * root)
{
    if(root)
    {
        return (count(root->left) + count(root->right);
    }
    //KARLA MENTIONED A POTENTIAL FAILURE WITH THE ELSE STATEMENT. ADDING JUST THE RETURN.
    return 0;
}


//STEP 2
int table::count_right_subtree()
{
    return count_right_subtree(root);
}

int table::count_right_subtree(node * root)
{
    // IF NONE EXIST -- BASE CASE
    if(!root)
    {
        return 0;
    }
    // IF RIGHT AND LEFT LEAVES EXIST;
    else if(root->right != nullptr && root->left != nullptr)
    {
        return (count_right_subtree(root->left) + count_right_subtree(root->right) + 1)
    }
    // IF JUST A LEFT LEAF EXISTS
    else if(root->right == nullptr && root->left != nullptr)
    {
        return(count_right_subtree(root->left);
    }
    // IF JUST A RIGHT LEAF EXISTS
    else
    {
        return(count_right_subtree(root->right) + 1;
    }
}


//STEP 3
int table::remove_largest(){
    return remove_largest(root);
}

int table::remove_largest(node * & root)
{
    // BASE CASE- THE RIGHT-MOST LEAF OF THE TREE.
    if(root->right == nullptr)
    {
        // NEED TO REARRANGE TREE
        if(root->left != nullptr)
        {
            int temp = root->data;
            root->data = (remove_largest(root->left);
            return temp;
        }
        // REMOVAL OF THE LEAF
        else
        {
            int temp = root->data;
            delete root;
            return temp;
        }
    }
    
    return(remove_largest(root->right);
}


//STEP 4
int table::sum()
{
    return sum(root);
}

int table::sum(node * root)
{

}

//STEP 5
int table::copy(table & to_copy){}
int table::copy(node * & destination, node * source){}


//STEP 6a Challenge
int table::create_full(){}
int table::create_full(node * & new_tree){}


//STEP 6b
bool table::is_full(){}
bool table::is_full(node * root){}


//STEP 6c
int table::display_largest(){}
int table::display_largest(node * root){}


//STEP 6d
int table::display_largest2(){}
int table::display_largest2(node * root){}



